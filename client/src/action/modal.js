import { MODAL } from '../reducers/type';

export const show = (Template) => {
    return dispatch => dispatch({
        type: MODAL.SHOW,
        status: true,
        template: Template
    });
}

export const hide = () => {
    return dispatch => dispatch({
        type: MODAL.HIDE,
        status: false
    });
}