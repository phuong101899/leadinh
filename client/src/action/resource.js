import client from './config';
import { RESOURCE } from '../reducers/type';

export const upload = (file) => {
    const formData = new FormData();
    formData.append('file', file);

    return dispatch => dispatch({
        type: RESOURCE.UPLOAD,
        payload: client.post('/resource', formData, {
            'Content-Type': 'multipart/form-data'
        })
    });
}

export const clearUrl = () => {
    return dispatch => dispatch({
        type: RESOURCE.CLEAR_URL
    });
}

export const addUrl = (url) => {
    return dispatch => dispatch({
        type: RESOURCE.ADD_URL,
        payload: url
    });
}
