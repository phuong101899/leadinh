import axios from 'axios';
import store from '../store';
import config from '../config';

const client = axios.create({
    baseURL: config.BASE_URL,
    headers: {
        'Content-Type': 'application/json'
    }
});

client.interceptors.request.use(config => {

    store.dispatch({
        type: 'LOADING',
        status: true
    });

    return config;
}, error => {
    return window.Promise.reject(error);
});

client.interceptors.response.use(response => {

    store.dispatch({
        type: 'LOADING',
        status: false
    });
    
    return response;
}, error => {
    return window.Promise.reject(error);
});

export default client;