import { POST } from '../reducers/type';
import client from './config';

export const  getList = (category) => {
    return dispatch => dispatch({
        type: POST.LIST,
        payload: client.get('/post', {params: {category}})
    });
}

export const postArticle = (data) => {
    return dispatch => dispatch({
        type: POST.POST,
        payload: client.post('/post', data)
    });
}