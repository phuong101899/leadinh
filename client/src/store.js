import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import reducers from './reducers';


const middleware = applyMiddleware(reduxThunk, promise);

export default createStore(reducers, middleware);