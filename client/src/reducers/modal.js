import { MODAL } from './type';

const defaultState = {
    status: false,
    template: null
};

export default (state = defaultState, action) => {
    switch(action.type) {

        case MODAL.SHOW: {
            return {
                ...state,
                status: action.status,
                template: action.template
            };
        }

        case MODAL.HIDE: {
            return {
                ...state,
                status: action.status
            };
        }

        default: {
            return state;
        }
    }
}