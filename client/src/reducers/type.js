export const POST = {
    LIST: 'LIST',
    POST: 'POST',
};

export const RESOURCE = {
    UPLOAD: 'UPLOAD',
    CLEAR_URL: 'CLEAR_URL',
    ADD_URL: 'ADD_URL'
};

export const MODAL = {
    SHOW: 'SHOW',
    HIDE: 'HIDE'
};