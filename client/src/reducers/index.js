import { combineReducers } from 'redux';
import post from './post';
import resource from './resource';
import modal from './modal';
import loading from './loading';
const reducers = {
    post,
    resource,
    modal,
    loading
};

export default combineReducers(reducers);