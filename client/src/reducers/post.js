import { POST } from './type';
import { ActionType } from 'redux-promise-middleware'

const defaultState = {
    list: [],
    isLoading: false
};

export default (state = defaultState, action) => {
    switch (action.type) {
        case `${POST.LIST}_${ActionType.Fulfilled}`: {
            let list = action.payload.data.items;
            return {
                ...state,
                list: list
            };
        }

        case `${POST.POST}_${ActionType.Pending}`: {
            return {
                ...state,
                isLoading: true
            };
        }

        case `${POST.POST}_${ActionType.Fulfilled}`: {
            return {
                ...state,
                isLoading: false
            };
        }

        default: {
            return state;
        }
    }
}