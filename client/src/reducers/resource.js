import { RESOURCE } from './type';
import { ActionType } from 'redux-promise-middleware'


const defaultState = {
    url: ''
};

export default (state = defaultState, action) => {
    switch (action.type) {
        case `${RESOURCE.UPLOAD}_${ActionType.Fulfilled}`: {
            let url = action.payload.data.url;
            return {
                ...state,
                url: url
            };
        }

        case RESOURCE.CLEAR_URL: {
            return {
                ...state,
                url: ''
            };
        }

        case RESOURCE.ADD_URL: {
            return {
                ...state,
                url: action.payload
            };
        }

        default: {
            return state;
        }
    }
}