const defaultState = {
    status: false
};

export default (state = defaultState, action) => {
    switch(action.type) {

        case 'LOADING': {
            return {
                ...state,
                status: action.status
            };
        }

        default: {
            return state;
        }
    }
}