import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Admin } from './containers/Admin';

import './App.css';


import Layout from './containers/Layout';

import store from './store';

class App extends Component {

	render() {
		return (
			<Provider store={store}>
				<Router>
					<Route path="/admin" component={Admin} />
					<Route path="/" exact={true} component={Layout} />
				</Router>
				
			</Provider>

		);
	}
}

export default App;
