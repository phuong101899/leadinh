import React, { Component, Fragment } from 'react';
import _ from 'lodash';

const linkVoid = '';
class Pagination extends Component {
   
    render() {
        let { pages, currentPage } = this.props;
        if(pages < 2) return null;

        let pageList = _.range(1, pages + 1);
        return (
            <Fragment>
                <div className="pagination">
                    <ul>
                        {_.map(pageList, (page, index) => (<li  key={index}><a href={linkVoid} className={currentPage === page ? 'active' : ''}>{page}</a></li>))}
                    </ul>
                </div>

            </Fragment>
        );
    }
}

export default Pagination;