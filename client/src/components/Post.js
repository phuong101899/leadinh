import React, { Component, Fragment } from 'react';
import { show, hide } from '../action/modal'
import { connect } from 'react-redux';
import Detail from './Detail';

import PropTypes from 'prop-types';

class Post extends Component {

    showDetail = () => {
        let images = this.props.images;

        let template = () => (<Detail images={images} />);
        this.props.show(template);
    }

    render() {
        console.log(this.props);
        return (
            <Fragment>
                <div className="grid">
                    <figure className="effect-oscar">
                        <img src={this.props.thumbnail} alt="Thumbnail" />
                        <figcaption>
                            <h4>{this.props.title}</h4>
                            <p>{this.props.description}</p>
                            <a className="smooth-redirect" onClick={e => this.showDetail()}>&nbsp;</a>
                        </figcaption>
                    </figure>
                </div>
            </Fragment>
        );
    }
}

Post.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    thumbnail: PropTypes.string.isRequired
};

const states = (state) => {
    return {

    };
}

const dispatches = {
    show,
    hide
};

export default connect(states, dispatches)(Post);