import React,{ Component, Fragment } from 'react';
import { connect } from 'react-redux';

class Loading extends Component {

    render() {
        if(!this.props.loading) return null;

        return (
            <Fragment>
                <div style={{position: 'fixed', top: 0, bottom: 0, left: 0, right: 0, backgroundColor: 'rgba(255, 255, 255, 0.7)', color: '#000', textAlign: 'center', zIndex: 9999}}>
                    <h4>Loading...</h4>
                </div>
            </Fragment>
        );
    }
}

const states = (state) => {
    return {
        loading: state.loading.status
    };
};

const dispatches = {};

export default connect(states, dispatches)(Loading);