import React, { Component, Fragment } from "react";
import _ from 'lodash';

class Detail extends Component {
	render() {
		return (
			<Fragment>
				<div className="modal-content">
					<div className="modal-body">
						{_.map(this.props.images, (item, index) => <img src={item} key={index} alt="Detail" />)}
					</div>
				</div>
			</Fragment>
		);
	}
}

export default Detail;
