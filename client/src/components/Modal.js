import React, { Component, Fragment } from "react";
import {hide} from '../action/modal';
import { connect } from "react-redux";

class Modal extends Component {
	componentDidMount() {
		if (this.props.status) {
			this.showModal();
		} else {
			this.hideModal();
        }
        
        window.$(this.modal).on('hidden.bs.modal',  () => {
            this.props.hide();
        });
    }
    
    componentDidUpdate() {
        if (this.props.status) {
			this.showModal();
		} else {
			this.hideModal();
		}
    }

	showModal = () => {
		window.$(this.modal).modal("show");
	};

	hideModal = () => {
		window.$(this.modal).modal("hide");
	};

	getTemplate = () => {
        if(this.props.template) {
            let Template = this.props.template;
            return (<Template />);
        }
		

		return (
			<div className="modal-content">
				<div className="modal-header">
					<button type="button" className="close" data-dismiss="modal">
				<span aria-hidden="true">×</span>
					</button>
					<h4 className="modal-title">Modal title</h4>
				</div>
				<div className="modal-body">
					<p>One fine body…</p>
				</div>
				<div className="modal-footer">
					<button
						type="button"
						className="btn btn-default"
						data-dismiss="modal"
					>
						Close
					</button>
					<button type="button" className="btn btn-primary">
						Save changes
					</button>
				</div>
			</div>
		);
	};

	render() {
		return (
			<Fragment>
				<div className="modal fade" tabIndex={-1} role="dialog" ref={e => (this.modal = e)} >
					<div className="modal-dialog" role="document" style={{width: 'auto', textAlign: 'center'}}>
                        {this.getTemplate()}
                    </div>
				</div>
			</Fragment>
		);
	}
}

const states = state => {
	return {
		status: state.modal.status,
		template: state.modal.template
	};
};

const dispatches = {
    hide
};

export default connect(
	states,
	dispatches
)(Modal);
