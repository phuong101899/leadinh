import React, { Component, Fragment } from 'react';

class Blog extends Component {
    constructor() {
        super();
    }

    showModal = (e) => {
        this.props.showModal(e);
    }

    closeModal = (e) => {
        this.props.closeModal(e);
    }

    render() {
        return (
            <Fragment>
                <section>
                    <figure className="effect-bubba">
                        <div className="it-title"><h2>Blog</h2></div>
                        <div className="bl-box" onClick={this.showModal}>
                            <div className="home-border" />
                        </div>
                        <div className="container top-img">
                            <div className="col-md-12 content-txt-work">
                                <div className="title-page"><h2>Blog</h2></div>
                            </div>
                        </div>
                        <div id="bl-blog-items" className="container bl-content">
                            <div className="col-md-8 blog-opinion">
                                <div className="blog-item first-child">
                                    <div className="blog-img"><img src="images/blog/blog_1.jpg" /></div>
                                    <div className="blog-desc box-text">
                                        <h2>Palais du Luxembourg - MY DREAM<span className="date-blog">September 14, 2014 / Blog</span></h2>
                                        <div className="blog-inf">
                                            <p>Not far off, one of the top things to do in Paris, the Palais du Luxembourg (Luxembourg Palace) houses the Sénat (Senate) which is the French parliament’s supreme chamber since 1958. The site that embraces today the Gardens and the Palais du Luxembourg (Luxembourg Palace) was originally a Roman camp.</p>
                                            <p className="blog-d-txt"> In 1257, when the Chartreux family settled in, this lavish place was a disreputable spot to a point where it was considered as cursed...</p>
                                            <i className="fa fa-tags" /> Blog <i className="fa fa-tags tag" /> Dream <i className="fa fa-tags tag" /> Travel
                    <a href="#" className="blog-read" data-panel="blog-1"> Read more</a>
                                        </div>
                                    </div>
                                </div>
                                <div className="blog-item">
                                    <div className="col-video">
                                        <div className="blog-img"><img src="images/blog/blog_2.jpg" /></div>
                                    </div>
                                    <div className="blog-desc box-text">
                                        <h2>Louvre Museum<span className="date-blog">September 14, 2015 / Blog</span></h2>
                                        <div className="blog-inf">
                                            <p>The Louvre is one of the most famous museums in the world, and it houses one of the most famous pieces of art in the world, Mona Lisa. Established in 1792, it is now the most visited museum in the world. With a history that directly involved Napoleon, you are sure to capture the essence of the history of Paris in the Louvre.</p>
                                            <p className="blog-d-txt">One thing which not a lot of people know is that Mona Lisa is not the best painting in the museum. A lot of people go to the museum just to see the painting...</p>
                                            <i className="fa fa-tags" /> Travel <i className="fa fa-tags tag" /> Blog <i className="fa fa-tags tag" /> Dream
                    <a href="#" className="blog-read" data-panel="blog-4"> Read more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 right-cat">
                                <div className="box-content right-position">
                                    <div className="blog-right-column">
                                        <form action="https://www.behance.net/leadinhdes68db" method="get" className="search-form">
                                            <input type="text" name="s" id="search" defaultValue placeholder="Search ..." />
                                            <button type="submit"><i className="fa fa-search" /></button>
                                        </form>
                                        <h2>Categories</h2>
                                        <ul>
                                            <li><a href="#"><i className="fa fa-angle-double-right" /> Travel</a></li>
                                            <li><a href="#"><i className="fa fa-angle-double-right" /> Video</a></li>
                                            <li><a href="#"><i className="fa fa-angle-double-right" /> Share</a></li>
                                            <li><a href="#"><i className="fa fa-angle-double-right" /> Music</a></li>
                                            <li><a href="#"><i className="fa fa-angle-double-right" /> Fashion</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="box-content blog-r-posts">
                                    <div className="blog-right-column">
                                        <h2>Recent Posts</h2>
                                        <ul>
                                            <li><a href="#"><i className="fa fa-angle-double-right" /> Palais du Luxembourg - MY DREAM </a></li>
                                            <li><a href="#"><i className="fa fa-angle-double-right" /> Louvre Museum</a></li>
                                            <li><a href="#"><i className="fa fa-angle-double-right" /> Cathedrale Notre-Dame de Paris</a></li>
                                            <li><a href="#"><i className="fa fa-angle-double-right" /> Jardin du Luxembourg </a></li>
                                            <li><a href="#"><i className="fa fa-angle-double-right" /> Saint Germain of Prés Church</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12 footer-social">
                                <a href="https://www.facebook.com/leadinhdesign" title="Facebook" target="_blank"><i className="fa fa-facebook" /></a>
                                <a href="#" title="Twitter" target="_blank"><i className="fa fa-twitter" /></a>
                                <a href="https://www.behance.net/leadinhdes68db" title="Behance" target="_blank"><i className="fa fa-behance" /></a>
                                <a href="#" title="LinkedIn+" target="_blank"><i className="fa fa-linkedin" /></a>
                                <a href="#" title="Pinterest" target="_blank"><i className="fa fa-pinterest" /></a>
                            </div>
                            <div className="col-md-12 copyright copyright-relative"><p>Lea Dinh Copyright © 2015 | All right reserved</p></div>
                        </div>
                        <span className="bl-icon bl-icon-close" onClick={this.closeModal}><i className="fa fa-times" /></span>
                    </figure>
                </section>
            </Fragment>
        );
    }
}

export default Blog;