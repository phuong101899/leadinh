import React, { Component, Fragment } from 'react';

class Profile extends Component {
    constructor() {
        super();
    }

    showModal = (e) => {
        this.props.showModal(e);
    }

    closeModal = (e) => {
        this.props.closeModal(e);
    }

    render() {
        return (
            <Fragment>
                <section>
                    <figure className="effect-bubba">
                        <div className="it-title"><h2>Lea Dinh!</h2></div>
                        <div className="bl-box" onClick={this.showModal}>
                            <div className="home-border"></div>
                        </div>
                        <div className="container top-img">
                            <div className="col-md-12 content-txt-work">
                                <div className="title-page"><h2>Lea Dinh!</h2></div>
                            </div>
                        </div>
                        <div className="container bl-content">

                            <div className="col-md-4 img-me">
                                <img src="images/my.png" />
                            </div>
                            <div className="col-md-8 about-desc">
                                <p className="nm-name">I'm <span>Lea Dinh</span></p>
                                <p>A design not through any formal training school &amp;certificate for design. But I always proud of my creative works . In spite of graduating from Information Technology major in Industrial University of Ho Chi Minh City, I turned to this career path of design by chance. It is not my major in the first sight, but I have passion and enthusiasm for this career. 7 years together is not so long but not so short time. When reviewing, I felt very happy when customers, partners and the others satisfied with and used my works .
                              </p>

                                <p className="slogan">" Life is short. Smile while you still have teeth. "</p>

                                <img className="signature" src="images/signature.png" />

                            </div>

                            <div className="col-md-12 data-box">

                                <ul className="about-data">
                                    <li className="flipInY animated"><strong>Full Name:</strong> DINH THI PHUONG LIEN </li>
                                    <li className="flipInY animated"><strong>ENG Name:</strong> LEA DINH </li>
                                    <li className="flipInY animated"><strong>Birthdate:</strong> 25/06/1990</li>

                                </ul>
                            </div>

                            <div id="startSkill" className="col-md-12 service-type">
                                <div className="col-md-4">

                                    <div className="service-item">
                                        <div className="icon">
                                            <span style={{backgroundImage: "url(images/s-1.png)"}}></span>
                                        </div>
                                        <div className="excerpt">
                                            <span className="icon-img" style={{backgroundImage: "url(images/s-1.png)"}}></span>
                                            <h4>Web designer</h4>
                                            <p>
                                                Currently, I am a creative executive of website design in company Canh Cam. My daily jobs are receiving briefs, information, analysing the requirements, outlining the wide frames, then designing the website layouts, responsives and providing to customer.
						            </p>
                                        </div>
                                    </div>

                                </div>

                                <div className="col-md-4">

                                    <div className="service-item">
                                        <div className="icon">
                                            <span style={{backgroundImage: "url(images/s-2.png)"}}></span>
                                        </div>
                                        <div className="excerpt">
                                            <span className="icon-img" style={{backgroundImage: "url(images/s-2.png)"}}></span>
                                            <h4>Teacher</h4>
                                            <p>
                                                I have 7 years of experiences on designing, and used to be a Photoshop teacher in Aptech center, as well as I am eager to share the experiences to youth. Therefore, if you need to improve your skills and thinking of design, I will save time to be your tutor.
						            </p>
                                        </div>
                                    </div>

                                </div>

                                <div className="col-md-4">

                                    <div className="service-item">
                                        <div className="icon">
                                            <span style={{backgroundImage: "url(images/s-3.png)" }}></span>
                                        </div>
                                        <div className="excerpt">
                                            <span className="icon-img" style={{backgroundImage: "url(images/s-3.png)"}}></span>
                                            <h4>Freelancer</h4>
                                            <p>
                                                In addition to my jobs that are website design and teacher, I want to develop further skills so I will receive all kinds of the design jobs, such as: banner, cover, poster, email marketing, card visit, catalogue…I hope that we can cooperate well. Thanks!
						            </p>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div className="container bl-content-exp">
                                <div className="col-md-12 experiences">
                                    <h2>PORTFOLIO<span>EXPERIENCES</span></h2>
                                </div>
                                <div className="col-md-6 experiences1">
                                    <div className="list-experiences">
                                        <h2><i className="fa fa-briefcase"></i>Nguyen Hoang Group<span className="data"> 2011 - 2012</span></h2>
                                        <p>I used to be an intern for 2 months, then I was an official employee in Nguyen Hoang Group. The main my job is designing softwares layouts  and finishing the design requests of Group. </p>
                                    </div>
                                    <div className="list-experiences">
                                        <h2><i className="fa fa-briefcase"></i>Taka Web Design Company <span className="data"> 2013 - 2016</span></h2>
                                        <p>Meet, exchange, advice, get information and design requirements directly from customers. Draw wireframe - demo website. Then prepare the document, scheduled for presentations, design point guard and convince customers not to change too much on the demo website for big clients such as: Vifon, Fashion TV, VFM .. . </p>
                                    </div>
                                </div>

                                <div className="col-md-6 experiences2">
                                    <div className="list-experiences">
                                        <h2><i className="fa fa-briefcase"></i> Univinet Web Design Company <span className="data"> 2012 - 2013</span></h2>
                                        <p>The main my job is direct exchange with account to receive information and brief to customer. After that I will follow website design timeline, design layouts and edit feedback to customer. </p>
                                    </div>
                                    <div className="list-experiences">
                                        <h2><i className="fa fa-briefcase"></i>Seldat Company<span className="data"> 2016 - Now</span></h2>
                                        <p>Receiving information design to customer  form business department. Design layout website follow request.  Support for the technical department, using wordprerss to edit, change image, content for layout. Designed the brand identity for companies large and small. Participate in training sessions designed by the company organization.</p>
                                    </div>
                                </div>



                            </div>



                            <div className="col-md-12 bt-skill">
                                <div className="col-md-6 work-box-txt book-img">
                                    <div className="big-number col-md-4">02</div><div className="big-desc2 col-md-8">Lea Dinh info <br />My skill</div>
                                </div>
                                <div className="col-md-6 my-skill">
                                    <div className="skills-area triggerAnimation animated">

                                        <div className="box-text-skill">

                                            <div className="skills-box">
                                                <div className="skills-progress">
                                                    <p>Photoshop<span>70%</span></p>
                                                    <div className="meter nostrips develope" data-percent="80%">
                                                        <p className="skillbar-bar" style={{width: "80%"}}></p>
                                                    </div>
                                                    <p>Illustrastor<span>60%</span></p>
                                                    <div className="meter nostrips design" data-percent="65%">
                                                        <p className="skillbar-bar" style={{width: "65%"}}></p>

                                                    </div>
                                                    <p>Indesign<span>30%</span></p>
                                                    <div className="meter nostrips branding" data-percent="50%">
                                                        <p className="skillbar-bar" style={{width: "50%"}}></p>

                                                    </div>
                                                    <p>Flash<span>50%</span></p>
                                                    <div className="meter nostrips branding" data-percent="60%">
                                                        <p className="skillbar-bar" style={{width: "60%"}}></p>

                                                    </div>
                                                    <p>Wordpress + HTML/CSS<span>20%</span></p>
                                                    <div className="meter nostrips web-applications" data-percent="40%">
                                                        <p className="skillbar-bar" style={{width: "40%"}}></p>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>

                            <div className="col-md-12 footer-social">
                                <a href="https://www.facebook.com/leadinhdesign" title="Facebook" target="_blank"><i className="fa fa-facebook"></i></a>
                                <a href="#" title="Twitter" target="_blank"><i className="fa fa-twitter"></i></a>
                                <a href="https://www.behance.net/leadinhdes68db" title="Behance" target="_blank"><i className="fa fa-behance"></i></a>
                                <a href="#" title="LinkedIn+" target="_blank"><i className="fa fa-linkedin"></i></a>
                                <a href="#" title="Pinterest" target="_blank"><i className="fa fa-pinterest"></i></a>
                            </div>
                            <div className="col-md-12 copyright copyright-relative"><p>Lea Dinh Copyright © 2015 | All right reserved</p></div>
                        </div>
                        <span className="bl-icon bl-icon-close" onClick={this.closeModal}><i className="fa fa-times"></i></span>
                    </figure>
                </section>
            </Fragment>
        );
    }
}

export default Profile;