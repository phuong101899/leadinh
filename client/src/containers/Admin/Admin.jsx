import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { upload, clearUrl, addUrl } from '../../action/resource'
import { postArticle } from '../../action/post';
import styles from './Admin.css';
import _ from 'lodash';
import * as ladda from 'ladda';

const CATEGORIRES = ['WEB', 'MOBILE', 'BANNER', 'LOGO', 'DASHBOARD', 'OTHER'];

class Admin extends Component {
    constructor() {
        super();

        this.state = {
            images: [],
            thumbnail: '',
            title: '',
            description: ''
        };
        
        this.loadingTarget = '';
        this.isThumbnail = false;
    }

    componentDidUpdate(){
        if(_.get(this.props, 'url'))
        {
            if(this.isThumbnail)
            {
                let url = this.props.url;
                this.props.clearUrl();
                this.isThumbnail = false;
                this.setState({thumbnail: url});
            }
            else
            {
                this.state.images.push(this.props.url);
                this.props.clearUrl();
                this.setState({images: this.state.images});
            }    
        }


        if(this.loadingTarget)
        {
            let loading = ladda.create(this.loadingTarget);

            if(this.props.isLoading)
            {
                loading.start();
            }
            else
            {
                loading.stop();
            }
        }

    }

    removeImage = (index) => {
        let images = this.state.images;
        images.splice(index, 1);
        this.setState({ images });
    }

    selectThumbnail = (e) => {
        var file = _.get(e.target, 'files.0');

        /** Submit to server */
        this.props.upload(file);
        this.isThumbnail = true;
    }

    selectImage = (e) => {
        var file = _.get(e.target, 'files.0');

        /** Submit to server */
        this.props.upload(file);
    }

    addImage = () => {
        let url = this.image.value;
        this.props.addUrl(url);

        this.image.value = ''; /** Clear field */
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.loadingTarget = window.document.getElementById('js-btn-submit');

        let {title, description, thumbnail, images, category} = this.state;
        this.props.postArticle({title, description, thumbnail, images, category});
    }

   

    render() {
        let images = this.state.images;
        var thumbnail = this.state.thumbnail;
        return (
            <Fragment>
                <div className={styles['container-admin']}>
                    <h1>Post Article</h1>
                    <form onSubmit={this.onSubmit}>
                        <div className="form=group">
                            <label htmlFor="category">Category</label>
                            <select name="category" id="category" className="form-control" onChange={e => this.setState({category: e.currentTarget.value})} required>
                                <option value=""></option>
                                {_.map(CATEGORIRES, (category, index) => <option key={index} value={category}>{category}</option> )}
                            </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="title">Title</label>
                            <input type="text" className="form-control" id="title" value={this.state.title} onChange={e => this.setState({title: e.currentTarget.value})} required />
                        </div>
                        <div className="form-group">
                            <label htmlFor="description">Description</label>
                            <input type="text" className="form-control" id="description" value={this.state.description} onChange={e => this.setState({description: e.currentTarget.value})} required />
                        </div>

                        <div className="form-group">
                            <label>Thumbnail</label>
                            <div className="input-group">
                                <input type="url" className="form-control" value={thumbnail} placeholder="Url image" onChange={e => this.setState({thumbnail: e.currentTarget.value})} required />
                                <input type="file" name="thumbnail" id="thumbnail" className="hidden" onChange={this.selectThumbnail} />
                                <span className="input-group-btn" >
                                    <label className="btn btn-primary" type="button" htmlFor="thumbnail">Browse</label>
                                </span>
                            </div>
                        </div>
                        <div>
                            <label className="form-group">Images</label>
                        </div>
                        {images.map((img, index) => (
                            <div className="form-group" key={index}>
                                <div className="input-group">
                                    <input type="text" className="form-control" value={img} readOnly={true}/>
                                    <span className="input-group-btn">
                                        <button className="btn btn-danger" type="button" onClick={e => this.removeImage(index)}>Remove</button>
                                    </span>
                                </div>
                            </div>
                        ))}

                        <div className="form-group">
                            <div className="input-group">
                                <input type="url" className="form-control" placeholder="Url image" ref={e => this.image = e} />
                                <span className="input-group-btn">
                                    <button className="btn btn-info" type="button" onClick={this.addImage}>Add</button>
                                    <label className="btn btn-primary" type="button" htmlFor="image">Browse</label>
                                </span>
                            </div>
                            <input type="file" name="image" onChange={this.selectImage} id="image" className="hidden" />
                        </div>

                        <div className="form-group">
                            <button type="submit" id="js-btn-submit" data-color="blue" className="btn btn-primary ladda-button">Submit</button>
                        </div>
                    </form>
                </div>
            </Fragment>
        );
    }
}


const states = (state) => {
    return {
        url: state.resource.url,
        isLoading: state.post.isLoading
    };
}

const dispatches = {
    upload,
    clearUrl,
    addUrl,
    postArticle
}


export default connect(states, dispatches)(Admin);