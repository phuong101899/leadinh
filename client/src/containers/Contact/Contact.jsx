import React, { Component, Fragment } from 'react';

class Contact extends Component {
    constructor() {
        super();
    }

    showModal = (e) => {
        this.props.showModal(e);
    }

    closeModal = (e) => {
        this.props.closeModal(e);
    }

    render() {
        return (
            <Fragment>
                <section>
                    <figure className="effect-bubba">
                        <div className="it-title"><h2>Contact</h2></div>
                        <div className="bl-box" onClick={this.showModal}>
                            <div className="home-border" />
                        </div>
                        <div className="container top-img">
                            <div className="col-md-12 content-txt-work">
                                <div className="title-page"><h2>Contact</h2></div>
                            </div>
                        </div>
                        <div className="container bl-content">
                            <div className="col-md-12 contact-form-title top-contact">
                                <h2>CONTACT <span>INFO</span></h2>
                            </div>
                            <p>Thank you so much that you are interested in my website. I’m sociable, friendly, eager to learn and exchange experiences. I hope, I can get acquainted and make friends with many people from various cultures all over the world. If you have the same thoughts as mine, have the same passion on creativity as mine, please contact with me. We can chat and share the experiences in our job as well as in our life. I am very delighted to have friends like you. Thank you so much, again!</p>
                            <div className="box-content contact">
                                <div className="col-xs-4 col-md-4 c-info"><p><i className="fa fa-phone" />+84 984 184 822</p></div>
                                <div className="col-xs-4 col-md-4 c-info"><p><i className="fa fa-skype" />leadinhdesign</p></div>
                                <div className="col-xs-4 col-md-4 c-info"><p><i className="fa fa-envelope-o" />leadinhdesign@gmail.com</p></div>
                                <div className="col-md-12 contact-form-title">
                                    <h2>GET IN TOUCH <span>WITH ME</span></h2>
                                </div>
                                <form id="contact_form" className="form email-form" method="post" autoComplete="off">
                                    <div className="col-md-6">
                                        <input type="text" name="subject" id="subject" className="input-contact" placeholder="Subject" />
                                        <input type="text" name="name" id="name" className="input-contact" placeholder="Name" />
                                        <input type="email" name="email" className="input-contact" id="email" placeholder="E-mail" />
                                    </div>
                                    <div className="col-md-6">
                                        <textarea name="message" id="message" className="input-contact" placeholder="Message" cols={30} rows={7} defaultValue={""} />
                                        <button type="submit" className="btn submit-contact"><i className="fa fa-envelope-o" />SUBMIT</button>
                                        <p className="success">Your message has been sent successfully.</p>
                                        <p className="error">E-mail must be valid and message must be longer than 20 characters.</p>
                                    </div>
                                </form>
                            </div>
                            <section id="section5" className="cd-section">
                                <div id="map_container">
                                    <iframe title="Google" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15673.908462697955!2d106.6885035345363!3d10.851269498816407!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3175284714b83a61%3A0xb3f07df94eab3868!2zVsaw4budbiBMw6BpLCBBbiBQaMO6IMSQw7RuZywgUXXhuq1uIDEyLCBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1449220691679" width={1090} height={405} frameBorder={0} style={{ border: 0 }} allowFullScreen />
                                </div>
                            </section>
                            <div className="col-md-12 footer-social">
                                <a href="https://www.facebook.com/leadinhdesign" title="Facebook" target="_blank" rel="noopener noreferrer"><i className="fa fa-facebook" /></a>
                                <a title="Twitter" target="_blank" rel="noopener noreferrer"><i className="fa fa-twitter" /></a>
                                <a href="https://www.behance.net/leadinhdes68db" title="Behance" target="_blank" rel="noopener noreferrer"><i className="fa fa-behance" /></a>
                                <a title="LinkedIn+" target="_blank" rel="noopener noreferrer"><i className="fa fa-linkedin" /></a>
                                <a title="Pinterest" target="_blank" rel="noopener noreferrer"><i className="fa fa-pinterest" /></a>
                            </div>
                            <div className="col-md-12 copyright copyright-relative"><p>Lea Dinh Copyright © 2015 | All right reserved</p></div>
                        </div>
                        <span className="bl-icon bl-icon-close" onClick={this.closeModal}><i className="fa fa-times" /></span>
                    </figure>
                </section>
            </Fragment>
        );
    }
}

export default Contact;