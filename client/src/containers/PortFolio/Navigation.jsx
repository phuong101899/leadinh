import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { getList } from '../../action/post';

import _ from 'lodash';

const CATEGORIRES = ['WEB', 'MOBILE', 'BANNER', 'LOGO', 'DASHBOARD', 'OTHER'];
class Navigation extends Component {
    constructor() {
        super();
        this.state = {
            currentCategory : _.get(CATEGORIRES, 0)
        };

    }

    componentDidMount(){
        let category = this.state.currentCategory;
        this.props.getList(category);
    }

    activeCategory = (category) => {
        this.setState({currentCategory: category});

        this.props.getList(category);
    }

    render() {
        return (
            <Fragment>
                <dl className="group">
                    <dt />
                    <dd>
                        <ul className="filter group">
                            {_.map(CATEGORIRES, (item, index) => <li key={index} className={`cl-effect-11 ${this.state.currentCategory === item ? 'current' : ''}`}><a data-hover={item} onClick={() => this.activeCategory(item)}>{item}</a></li>)}
                        </ul>
                    </dd>
                </dl>
            </Fragment>
        );
    }
}

const states = (state) => {
    return {

    };
};

const dispatchs = {
    getList
};

export default connect(states, dispatchs)(Navigation);