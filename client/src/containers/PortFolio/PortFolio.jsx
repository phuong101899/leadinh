import React, { Component, Fragment } from 'react';
import Navigation from './Navigation';
import List from './List';
import Pagination from '../../components/Pagination';

const SOCIAL_LINKS = {
    linkedin: '',
    pinterest: '',
    twitter: ''
};
class PortFolio extends Component {

    showModal = (e) => {
        this.props.showModal(e);
    }

    closeModal = (e) => {
        this.props.closeModal(e);
    }

    render() {
        return (
            <Fragment>
                <section>
                    <figure className="effect-bubba">
                        <div className="it-title"><h2>Portfolio</h2></div>
                        <div className="bl-box" onClick={this.showModal}>
                            <div className="home-border" />
                        </div>
                        <div className="container top-img">
                            <div className="col-md-12 content-txt-work">
                                <div className="title-page"><h2>Portfolio</h2></div>
                            </div>
                        </div>
                        <div className="container bl-content">
                            
                            <div className="col-md-12 experiences">
                                <h2>My Latest <span>Works</span></h2>
                            </div>
                            <div className="col-md-12 itens-work">
                                <Navigation />
                                <List />
                                <Pagination pages={3}/>
                            </div>
                            <div className="col-md-12 footer-social">
                                <a href="https://www.facebook.com/leadinhdesign" title="Facebook" target="_blank" rel="noopener noreferrer"><i className="fa fa-facebook" /></a>
                                <a href={SOCIAL_LINKS.twitter} title="Twitter" target="_blank" rel="noopener noreferrer"><i className="fa fa-twitter" /></a>
                                <a href="https://www.behance.net/leadinhdes68db" title="Behance" target="_blank" rel="noopener noreferrer"><i className="fa fa-behance" /></a>
                                <a href={SOCIAL_LINKS.linkedin} title="LinkedIn+" target="_blank" rel="noopener noreferrer"><i className="fa fa-linkedin" /></a>
                                <a href={SOCIAL_LINKS.pinterest} title="Pinterest" target="_blank" rel="noopener noreferrer"><i className="fa fa-pinterest" /></a>
                            </div>
                            <div className="col-md-12 copyright copyright-relative"><p>Lea Dinh Copyright © 2015 | All right reserved</p></div>
                        </div>
                        <span className="bl-icon bl-icon-close" onClick={this.closeModal}><i className="fa fa-times" /></span>
                    </figure>
                </section>
            </Fragment>
        );
    }
}

export default PortFolio;