import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Post } from '../../components';

class List extends Component {
    
    render() {
        
        return (
            <Fragment>
                <div className="portfolio">
                {
                    this.props.list.map((item, index) => (
                            <div key={index} className="cat1 col-md-4 item" data-panel="panel-1">
                                <Post  title={item.title} description={item.description} thumbnail={item.thumbnail} images={item.images} /> 
                            </div>
                        ) 
                    )
                }
                </div>
            </Fragment>
        );
    }
}

const states = (state) => {
    return {
        list: state.post.list
    };
}

const dispatches = {
    
}

export default connect(states, dispatches)(List);