import React, { Component, Fragment } from 'react';
import { Profile } from './Profile';
import { PortFolio } from './PortFolio';
import { Blog } from './Blog';
import { Contact } from './Contact';
import { Modal } from '../components';
import Loading from '../components/Loading';


class Layout extends Component {
    componentDidMount() {

	}

	showModal = (e) => {
		window.$(e.currentTarget).closest('section').addClass('bl-expand bl-expand-top');
		window.$(this.mainEl).addClass('bl-expand-item');
	}

	closeModal = (e) => {
		window.$(e.currentTarget).closest('section').removeClass('bl-expand bl-expand-top');
		window.$(this.mainEl).removeClass('bl-expand-item');
    }
    
    render() {
        return (
            <Fragment>
                <div className="container-site">
                    <div id="bl-main" className="bl-main" ref={e => this.mainEl = e}>
                        <Profile showModal={this.showModal} closeModal={this.closeModal} />
                        <PortFolio showModal={this.showModal} closeModal={this.closeModal} />
                        <Blog showModal={this.showModal} closeModal={this.closeModal} />
                        <Contact showModal={this.showModal} closeModal={this.closeModal} />
                        <Modal />
                    </div>
                </div>
                <Loading />
            </Fragment>
        );
    }
}

export default Layout;