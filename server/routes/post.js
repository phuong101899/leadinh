var express = require('express');
var router = express.Router();
const connection = require('../database');
const shortid = require('shortid');
const _ = require('lodash');

/* GET home page. */
router.get('/', (req, res) => {

    let { page, category } = req.query;
    page = page || 1;
    let size = 12;

    let start = (page - 1) * size;
    let request = 2;
    let total = 0;
    let items = [];
    

    const doneQuery = () => {
        res.json({
            status: 'success',
            items,
            total,
            pages: Math.ceil(total/size)
        });
    }

    connection.query('SELECT * FROM post WHERE category=? LIMIT ?,?', [category, start, size], (error, results) => {
        if(!error)
        {
            _.each(results, item => {
                item.images = JSON.parse(item.images);
            });
            items = results;

            request--;
            if(request === 0) doneQuery();
        }
        else
        {
            res.json({
                status: 'error',
                error: error
            });
        }
    });
   
    connection.query('SELECT COUNT(*) as total FROM post WHERE category=?', [category], (error, results) => {
        if(!error)
        {
            let row = _.first(results);
            total = _.get(row, 'total');

            request--;
            if(request === 0) doneQuery();
        }
    });
    
});

router.post('/', (req, res, next) => {
    let { title, description, thumbnail, images, category } = req.body;
    let id = shortid.generate();

    images = JSON.stringify(images);

    connection.query('INSERT INTO post SET ?', {id, title, description, thumbnail, images, category }, (error, results, fields) => {
        if (!error)
        {
            

            setTimeout(() => {
                res.json({
                    success: true,
                    message: 'Saved successfully!'
                });
            }, 3000);
        }
        else
        {
            res.json({
                success: false,
                message: error
            })
        }
    });
    
});

module.exports = router;
