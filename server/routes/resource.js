const express = require('express');
const router = express.Router();
const connection = require('../database');
const multer = require('multer');
const storage = multer.memoryStorage();
const resource = multer({dest: 'data/'});
const _ = require('lodash');



router.post('/', resource.single('file'), (req, res, next) => {
    let file = req.file;

    if(_.includes(['image/png', 'image/jpg', 'image/jpeg'], file.mimetype))
    {
        /** Process upload */
        let data = {
            filename: file.filename,
            mimetype: file.mimetype,
            originalname: file.originalname,
            path: file.path,
            destination: file.destination
        };

        connection.query('INSERT INTO file SET ?', {...data}, (error, results, fields) => {
            if (!error)
            {
                let url =`${req.protocol}://${req.hostname}:4000/${file.filename}`;
                res.json({
                    success: true,
                    url: url,
                    message: 'Saved successfully!'
                });
            }
            else
            {
                res.json({
                    success: false,
                    message: error
                })
            }
        });
    }
    else
    {
        res.json({
            success : false,
            msg : 'File type is invalid'
        });
    }
});

module.exports = router;